/* Proxy for ssh+SOCKS proxy

When testing on a Mac, reload SOCKS proxy this way:

networksetup -listallnetworkservices | awk 'NR>1' | while read SERVICE ; do
  if networksetup -getautoproxyurl "$SERVICE" | grep '^Enabled: Yes' >/dev/null; then
    networksetup -setautoproxystate "$SERVICE" off
    networksetup -setautoproxystate "$SERVICE" on
    echo "$SERVICE" bounced.
  fi
done

In Chrome, reload using the button at chrome://net-internals/#proxy

*/

function FindProxyForURL(uri, host) {

  // Log to chrome://net-internals/#events for debugging.
  alert("ssh-proxy.pac: STARTING")

  // Host matches that use the HTTP proxy.
  var URLs = [
    "128.55.168.*",
    "*.nersc.org",
    "*.nersc.gov",
  ];

  // Check all host patterns and network masks.
  for (var i = 0; i < URLs.length; i++) {
    if (shExpMatch(host, URLs[i])) {
      return "SOCKS 127.0.0.1:1626; DIRECT";
    }
  }
}
