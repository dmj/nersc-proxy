Accessing NERSC using a SOCKS5 proxy
====================================

1. Create a `.pac` file. See the included example. See configuration hints at :

  * https://developer.mozilla.org/en-US/docs/Web/HTTP/Proxy_servers_and_tunneling/Proxy_Auto-Configuration_(PAC)_file

3. Use Apple's built in webserver to serve the file

Copy the `.pac` file to the Apache DocumentRoot, and then start the webserver.
This will make the file available at `http://localhost/nersc-proxy/ssh-proxy.pac`.

    sudo chown $USER /Library/WebServer/Documents/
    cd /Library/WebServer/Documents/
    git clone https://gitlab.nersc.gov/stefanl/nersc-proxy.git

    sudo launchctl load -w /System/Library/LaunchDaemons/org.apache.httpd.plist

2. Load the PAC file into the macOS network configuration.
   * Open Apple Menu > System Preferences > Wi-Fi > Advanced > Proxies.
   * Check "Automatic Proxy Configuration"
   * Under "Proxy Configuration File", add the URL `http://localhost/nersc-proxy/ssh-proxy.pac`.

2. Configure SSH to use the proxy. In `~/.ssh/config`, add configuration like
   this. SSH will open port 1626 and will then use the sg-crt nodes as a proxy. 

        Host *
            ControlMaster auto
            ControlPath ~/.ssh/control-master.%r@%h:%p
            TCPKeepAlive no
            ServerAliveCountMax 10
            ServerAliveInterval 15

        Host sg-crt
            Hostname sg-crt.nersc.gov
            PKCS11Provider /usr/local/Cellar/opensc/0.20.0/lib/opensc-pkcs11.so
            DynamicForward 1626


5. Activate the proxy. In a new window, simply SSH to `sg-crt` (Use `sg3` or `sg4` as backup hosts.)

# Making changes and reloading

After making changes to the PAC file, macOS sometimes doesn't recognize the changes. To force macOS to reload the file, do this:

    stefanl@stefanl:nersc-proxy $ ./reload-proxy.sh
    Wi-Fi bounced.
    stefanl@stefanl:nersc-proxy $

# Troubleshooting

## Check the Apache logfiles under /private/var/log/apache2/

    stefanl@stefanl:~ $ tail -f /private/var/log/apache2/access_log 
    ::1 - - [20/Mar/2020:16:59:09 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:28 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:28 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:28 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:29 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:29 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:30 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:30 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:30 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687
    ::1 - - [20/Mar/2020:16:59:30 -0700] "GET /ssh-proxy.pac HTTP/1.1" 200 1687

## Is the network interface loading the PAC file?

Look for "Enabled: Yes"

    stefanl@stefanl:nersc-proxy $ networksetup -listallnetworkservices | awk 'NR>1' | while read SERVICE ; do echo "Network Interface: $SERVICE"; networksetup -getautoproxyurl "$SERVICE" | grep '^Enabled: '; done
    Network Interface: USB 10/100/1000 LAN
    Enabled: No
    Network Interface: Display Ethernet
    Enabled: No
    Network Interface: Display FireWire
    Enabled: No
    Network Interface: Wi-Fi
    Enabled: Yes
    Network Interface: Bluetooth PAN
    Enabled: No
    Network Interface: Thunderbolt Bridge
    Enabled: No
    Network Interface: NERSC Management VPN
    Enabled: No
    stefanl@stefanl:nersc-proxy $

## Is chrome using the proxy?

Check chrome://net-internals/#proxy

## Chrome logs

Start at chrome://net-export/ & load into netlog-viewer.

Under 'Proxy', you should see this message:

    Effective proxy settings

      PAC script: http://localhost/nersc-proxy/ssh-proxy.pac

Under 'Events' you should see messages with a description of `PAC_JAVASCRIPT_ALERT` and message like thisfrom the PAC file:

    t=333791926 [st=0]  PAC_JAVASCRIPT_ALERT
                        --> message = "ssh-proxy.pac: gitlab.nersc.gov Proxied to SOCKS!"

# Other applications

Do a `git push` using a proxy one time without reconfiguring Git:

    ALL_PROXY=socks5://127.0.0.1:1626 git push -u origin master
