#! /usr/bin/env bash

# Borrowed from https://q1f4mmprz7gko.wordpress.com/2015/07/10/forcing-mac-os-x-and-safari-to-reload-a-pac-file/

# After changing the PAC file, MacOS won't always detect the changes. So reload it this way.

networksetup -listallnetworkservices | awk 'NR>1' | while read SERVICE ; do
  if networksetup -getautoproxyurl "$SERVICE" | grep '^Enabled: Yes' >/dev/null; then
    networksetup -setautoproxystate "$SERVICE" off
    networksetup -setautoproxystate "$SERVICE" on
    echo "$SERVICE" bounced.
  fi
done
